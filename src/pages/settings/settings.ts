import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { WeatherProvider } from '../../providers/weather/weather';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  city: string;
  country: string;
  cityList: any[];
  showCities: boolean;
  cityFilter: string;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private weatherProvider: WeatherProvider,
    private storage: Storage) {
     
      this.showCities=false;
    this.storage.get('location').then(val => {
      if (val != null) {
        let location = JSON.parse(val);
        this.city = location.city;
        this.country = location.country;
      }
      else {
        this.city = 'Mangalore';
        this.country = 'India';
      }
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

//getcities
getCities(event)
{

   this.cityFilter = event.target.value;

console.log("CITY FILTER: " +this.cityFilter);
if(this.cityFilter==""){this.showCities=false;} else this.showCities=true;

this.weatherProvider.getFilteredCity(this.cityFilter).
subscribe((res)=>{ console.log(res.RESULTS); this.cityList= res.RESULTS},(err)=>{console.log("error!!")});

}

trackCity(selectedCity){


  console.log("selected city: " +selectedCity);
  this.cityFilter="";
  this.showCities=false;
  this.city=selectedCity.split(",")[0].trim();
  this.country=selectedCity.split(",")[1].trim();
}

HideList(){this.showCities=false;}

  saveForm() {
    let location = {
      city: this.city,
      country: this.country
    }
    console.log(location);
    this.storage.set('location', JSON.stringify(location));
    this.cityList=null;
    alert("Settings successfully saved. Please click on the Home tab at the bottom!");
    //this.navCtrl.push(HomePage);

    
  }
}