import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class WeatherProvider {
  apiKey = 'af12f9f5fce25337';
  url;
  searchurl;
  constructor(public http: Http) {
    console.log('Hello WeatherProvider Provider');
    this.url = 'http://api.wunderground.com/api/' + this.apiKey + '/conditions/q';
    this.searchurl = 'http://cors-anywhere.herokuapp.com/http://autocomplete.wunderground.com/aq?query='
  }

  getWeather(city, country) {
    return this.http.get(this.url + '/' + country + '/' + city + '.json')
      .map((res: Response) => res.json())
      .catch(this._handleError);
  }

  _handleError(error: Response) {
    return Observable.throw(error.statusText + " | Weather API Error!");
  }

  getFilteredCity(searchFilter) {
    return this.http.get(this.searchurl + searchFilter + "&h=0&cities=1")
      .map((res: Response) => res.json())
      .catch(this._handleError);
  }

}
