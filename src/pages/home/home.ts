import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { WeatherProvider } from '../../providers/weather/weather';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  weather: any;
  errorMsg: string;
  location:
  {
    city: string,
    country: string
  }
  constructor(public navCtrl: NavController,
    private weatherProvider: WeatherProvider,
    private storage: Storage) {
    this.errorMsg = null;
  }


  ionViewWillEnter() {
    this.errorMsg = null;
    this.showWeather();
  }

  showWeather() {
    this.storage.get('location').then((val => {
      if (val != null) {
        this.location = JSON.parse(val);
      }
      else {
        this.location = {
          city: 'Mangalore',
          country: 'India'
        }
      }
      this.weatherProvider.getWeather(this.location.city, this.location.country)
        .subscribe((weather) => {
          if (weather) {
            console.log(weather.current_observation);
            if (JSON.stringify(weather.current_observation)) {
              this.weather = weather.current_observation;
              this.weather.icon_url = 'http://icons.wxug.com/i/c/v4/' + this.weather.icon + '.svg';

              let x = document.getElementById('contentBlock');
              let classList = x.classList;
              while (classList.length > 0) { classList.remove(classList.item(0)); }
              x.classList.add("content");
              x.classList.add("content-md");
              switch (this.weather.weather) {
                case "Clear":
                case "Sunny":
                case "Mostly Sunny":
                  {
                    x.classList.add("clear");
                    break;
                  }


                case "Partly Cloudy":
                case "Partly Sunny":
                  {

                    x.classList.add("partlycloudy");

                    break;
                  }
                case "Mostly Cloudy":
                case "Cloudy":
                  {
                    x.classList.add("mostlycloudy");

                    break;
                  }
                case "Chance of a Thunderstorm":
                  {
                    x.classList.add("tstormschance");
                    break;
                  }
                case "Thunderstorm":
                  {

                    x.classList.add("tstorms");
                    break;
                  }
                case "Hazy":
                case "Haze":
                case "Fog":
                  {
                    x.classList.add("hazy");
                    break;
                  }
                case "Chance of Rain":
                case "Rain":
                  {
                    x.classList.add("rain");
                    break;
                  }
                case "Chance of Snow":
                case "Snow":

                  {
                    x.classList.add("snow");
                    break;
                  }
                case "Sleet":
                case "Chance of Sleet":
                case "Flurries":
                case "Chance of Flurries":
                  {

                    x.classList.add("sleet");
                    break;
                  }
              }
              console.log(x.classList);
            }
            else { this.errorMsg = "Weather information not available for this location!"; console.log(this.errorMsg); }
          }
          else { this.errorMsg = "Weather information not available for this location!"; console.log(this.errorMsg); }
        }
        , (err: Response) => { this.errorMsg = err.statusText || "Error getting Weather!"; console.log(this.errorMsg); }
        )
    }));

  }

}